import * as React from "react";
import Layout from "../components/layout";
import { StaticImage } from "gatsby-plugin-image";

const IndexPage = () => {
  return (
    <Layout pageTitle="Home">
      <p>This is the home page</p>
      <StaticImage alt="A static Image that will never be changed" src="../images/StaticImage.jpg"/>
    </Layout>
  );
};

export default IndexPage;
